import numpy as np 
import pandas as pd 

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import GridSearchCV


df = pd.read_csv("sonar.csv",header =None)
print(df.shape)

X = df.drop(columns = 60).values
Y = df[60].values
print(type(X))

print(X.shape, Y.shape)

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state = 42)

#Random Forests
clf1=RandomForestClassifier()
params1 = {'criterion' : ['gini', 'entropy'], 'n_estimators' : [100,150,200], 'min_samples_leaf' : [1,2,3], 'min_samples_split' : [3,4,5,6,7], 'random_state' : [42],'n_jobs': [-1]}
model1 = GridSearchCV(clf1, param_grid=params1, n_jobs=-1)
model1.fit(X_train,Y_train)
Y_pred = model1.predict(X_test)
print("***Random Forest Classifier***")
print("Best Hyperparameters: ", model1.best_params_)
print("Accuracy:", accuracy_score(Y_test, Y_pred))
print("Confusion Matrix: ", confusion_matrix(Y_test, Y_pred))

#Decision Trees
clf2 = DecisionTreeClassifier()
params2 = {'max_features' : ['auto', 'sqrt', 'log2'], 'min_samples_split':[5,6,7,8], 'min_samples_leaf': [4,5,6], 'random_state' : [42]}
model2 = GridSearchCV(clf2, param_grid = params2,n_jobs=-1)
model2.fit(X_train, Y_train)
Y_pred = model2.predict(X_test)
print("***Decision Tree Classifier***")
print("Best Hyperparameters:",model2.best_params_)
print("Accuracy:", accuracy_score(Y_test, Y_pred))
print("Confusion Matrix: ", confusion_matrix(Y_test, Y_pred))

#SVC
clf3 = SVC(gamma='auto')
params3 = {'C' : [6,7,8,9,10,11,12], 'kernel' : ['linear' , 'rbf']}
model3 = GridSearchCV(clf3, param_grid=params3, n_jobs=-1)
model3.fit(X_train, Y_train)
print("***SVC Classifier***")
print("Best Hyperparameters: ", model3.best_params_)
Y_pred = model3.predict(X_test)
print("Accuracy:", accuracy_score(Y_test, Y_pred))
print("Confusion Matrix: ", confusion_matrix(Y_test, Y_pred))
